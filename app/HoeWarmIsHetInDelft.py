
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from decimal import Decimal
import logging
from os import environ, getenv


class Parser(object):
    """
    Parser class:  takes a 3 constructor parameters:
        returns: populates .data property with decimal centigrade temperature as string

    html - html document as string
    element - html element location of desired content
    attrs - dictionary of attributes, in our case id and class further pointing to desired content
    """
    log = None
    data = None

    def __init__(self, in_html, in_data_element, in_data_attrs):
        try:
            self.log = logging.getLogger(self.__class__.__name__)
            soup = BeautifulSoup(in_html, features="html.parser")
            temp_centigrade_raw = soup.find(in_data_element, attrs=in_data_attrs).text
            self.data = temp_centigrade_raw.replace(" ", "").rstrip(temp_centigrade_raw[-2:])

            self.log.debug("PARSER END DATA : {0}".format(self.data))
        except Exception as e:
            self.log.error("Failed to retrieve data from html")
            self.log.debug(e)
            exit(10)


class Retriever(object):
    """
    Retriever class: takes a 1 constructor parameters:
        returns: populates .data property with retrieved html string of the retrieved document

    url - location of content to be parsed
    """
    log = None
    data = None

    def __init__(self, in_url):
        self.log = logging.getLogger(self.__class__.__name__)

        try:
            # when launching firefox, do not show UI
            options = Options()
            options.headless = True

            driver = webdriver.Firefox(options=options)
            driver.get(in_url)
            self.wait_for_ajax(driver)
            self.data = driver.page_source
        except Exception as e:
            self.log.error("Failed to retrieve data from url")
            self.log.debug(e)
            exit(20)

    # ref: https://stackoverflow.com/questions/24053671/webdriver-wait-for-ajax-request-in-python
    def wait_for_ajax(self, driver):
        wait = WebDriverWait(driver, 15, poll_frequency=2)
        try:
            wait.until(lambda driver: driver.execute_script('return jQuery.active') == 0)
            wait.until(lambda driver: driver.execute_script('return document.readyState') == 'complete')
        except Exception as e:
            pass


class Temperature(object):
    """
    Temperature class: takes a 1 constructor parameter:
        returns: populates .temperature_centigrade property with a decimal centigrade temperature value
                 exposes two methods for retrieval of temperature:  .as_centigrade_rounded_string  and .as_centigrade_sentence

    url - location of content to be parsed
    """
    log = None
    temperature_centigrade = None

    def __init__(self, in_temp_raw_string):
        self.log = logging.getLogger(self.__class__.__name__)
        try:
            temp_centigrade_string = in_temp_raw_string.replace(" ", "").rstrip(in_temp_raw_string[-2:])
            self.temperature_centigrade = Decimal(temp_centigrade_string)
        except Exception as e:
            self.log.error("Failed to parse temperature value")
            self.log.debug("EXCEPTION : {0}".format(e))
            self.log.debug("DATA      : {0}".format(self.temperature_centigrade))
            exit(31)

    def as_centigrade_rounded_string(self):
        output = None
        try:
            output = str(round(self.temperature_centigrade,0))
        except Exception as e:
            self.log.error("Failed to round temperature value")
            self.log.debug("EXCEPTION : {0}".format(e))
            self.log.debug("DATA      : {0}".format(self.temperature_centigrade))
            exit(32)
        return output

    def as_centigrade_sentence(self):
        output_str = self.as_centigrade_rounded_string()
        if output_str:
            return "{0} degrees Celsius".format(output_str)
        else:
            return "service unavailable"


def main():
    # Enabled if Env: DEBUG=TRUE
    # control possible outside of containery
    set_debugging()

    # using embedded variables for the example
    # abstracted so can be used elsewhere

    url = 'https://weerindelft.nl/WU/55ajax-dashboard-testpage.php'
    data_element = "span"
    data_attrs = {"class": "ajax", "id": "ajaxtemp"}

    r = Retriever(url)
    p = Parser(r.data, data_element, data_attrs)
    temperature = Temperature(p.data)
    print(temperature.as_centigrade_sentence())


def set_debugging():
    """
    if DEBUG=TRUE from environment variable or using --env DEBUG=TRUE from docker run,
    will make visible all debug messages from this and other inherited components. Skipped by default.
    """
    if "DEBUG" in environ:
        if getenv("DEBUG").upper() == "TRUE":
            logging.basicConfig(level=logging.DEBUG)


if __name__ == "__main__":
    main()
