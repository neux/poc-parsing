# POC-PARSING

## Task
Retrieve temperature for city of Delft, NL from main page of http://www.weerindelft.nl/

## Considerations & Research ##
* No API. Falling back on HTML parsing
* Main page only temperature is in iframe https://weerindelft.nl/WU/55ajax-dashboard-testpage.php
* IFrame component utilizing Javscript Ajax delivery https://weerindelft.nl/WU/ajaxWDwx.js relying on ActiveXObject("Microsoft.XMLHTTP"), evil. 
* Ajax component is parsing environment from various locations. Temperature is provided in the clientraw.txt serving from the same site.
* Falling back on BeautifulSoup with Selenium driver to retrieve dynamic content.

#### ClientRaw.txt data file (where the data comes from) ####
Example of the data for the iframe component parsed in javascript 
```
12345 8.3 7.0 341 11.3 55 1019.4 0.0 10.0 236.2 0.00 0.00 19.6 33 100.0 5 0.0 0 0 0.0 -100.0 255.0 -100.0 -100.0 -100.0 -100.0 -100 -100 -100 19 12 49 weerindelft_-19:12:49 0 70 14 5 0.00 0.00 100 100 100 100 100 11.3 9.8 12.2 4.6 5 Overcast_and_gloomy/Dry -0.1 10 7 7 7 7 6 6 5 11 15 14 14 13 10 5 4 4 10 10 8 17.4 2.6 3593.4 14/5/2020 10.6 2.4 12.2 4.6 0.8 4 5 5 6 6 5 8 4 4 4 11.6 11.6 11.6 11.6 11.3 11.4 11.3 11.4 11.3 11.3 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 12.2 4.6 11.3 12.4 0 --- --- 4 0 0 -100.0 -100.0 8 -100 -100 -100 -100 288.0 19.8 18.6 11.3 1020.8 1016.9 16 18:36 16:27 18.2 3.3 3.0 -0.7 10 2020 -12.0 -1 1 1 353 31 1 31 29 357 352 26 3 341 0.0 255.0 3.3 7.5 51.97944 -4.34611 0.0 72 45 0.0 01:34 0.0 0.0 0.0 0.0 0.0 0.0 91.7 17:01 06:11 1 !!C10.37S103!! 
```

Just to demonstrate where the data comes from and how its assembled out of the string above. I will paste the code fragment from ajaxWDwx.js 
```
		//Temperature
		temp = convertTemp(clientraw[4]);
		set_ajax_obs("ajaxtemp", temp.toFixed(1) + uomTemp);
		set_ajax_obs("ajaxtempNoU", temp.toFixed(1));
		set_ajax_obs("gizmotemp", temp.toFixed(1) + uomTemp);
		set_ajax_obs("ajaxbigtemp",temp.toFixed(0) + uomTemp);

		templast = convertTemp(clientraw[90]);
		temparrow = ajax_genarrow(temp, templast, '', 
			 langTempRising+uomTemp+langTempLastHour,
			 langTempFalling+uomTemp+langTempLastHour,1)
		
		set_ajax_obs("ajaxtemparrow",temparrow); 
		set_ajax_obs("gizmotemparrow",temparrow); 
		   
	    temprate = temp - templast;
		temprate = temprate.toFixed(1);
		if (temprate > 0.0) { temprate = '+' + temprate;} // add '+' for positive rates
		set_ajax_obs("ajaxtemprate",temprate + uomTemp);
		set_ajax_obs("gizmotemprate",temprate + uomTemp);

		tempmax = convertTemp(clientraw[46]);
		set_ajax_obs("ajaxtempmax",tempmax.toFixed(1) + uomTemp);

		tempmin = convertTemp(clientraw[47]);
		set_ajax_obs("ajaxtempmin",tempmin.toFixed(1) + uomTemp);
		
		thermometerstr = langThermoCurrently +  + temp.toFixed(1) + uomTemp + 
		  ", " + langThermoMax + tempmax.toFixed(1) + uomTemp +
		  ", " + langThermoMin + tempmin.toFixed(1) + uomTemp;

		set_ajax_obs("ajaxthermometer",
			"<img src=\"" + thermometer + "?t=" + temp.toFixed(1) + "\" " +
				"width=\"54\" height=\"170\" " +
				"alt=\"" + thermometerstr + "\" " +
				"title=\"" + thermometerstr + "\" />" );
```

It is truly horrific way of assembly of services providing data. Perfect for a training of how not to do it. Fortunately we can bypass this and jump directly into retrieving required information by parsing the webpage.


## Solution ##
I have decided to use BeautifulSoup4 python component with Selenium Firefox driver. This will allow me to retrieve data injected dynamically into the browser by Ajax/Javascript above, and still parse/process it with ease of BS4. Data on the page is in a single element/id/class combination with no detectable duplicates. Temperature provided is already in centigrade.

#### Retriever Class ####
Retriever Class is designed to contact any server from provided URL and bring back html string

#### Parser Class ####
Parser Class is designed to retrieve specific information from HTML string. Constructor takes html text, html-element and html-attributes like id and class further defining the position of desired content.

#### Temperature Class ####
Temperature Class is a business requirement class. It will parse retrieved value into a internal decmial value. Two methods exist for temperature retrieval. First for rounded to int temperature centigrade. Second for full sentence as rounded to int temperature centigrade. 

### Installation using provided .Dockerfile (self-build) ###
* Using elgalu/selenium container as base (50M+ pulls, last update 3 days ago) has all the required elements. Extras are heavy but for the POC it should do just fine.
Build Command: `docker build -t poc-parsing:dev .`

### Run Container with DEBUGGING
Run Command: `docker run --name poc-parsing --env DEBUG=TRUE poc-parsing:dev`
Example Output:
``` 
DEBUG:selenium.webdriver.remote.remote_connection:POST http://127.0.0.1:41481/session {"capabilities": {"firstMatch": [{}], "alwaysMatch": {"browserName": "firefox", "acceptInsecureCerts": true, "moz:firefoxOptions": {"args": ["-headless"]}}}, "desiredCapabilities": {"browserName": "firefox", "acceptInsecureCerts": true, "marionette": true, "moz:firefoxOptions": {"args": ["-headless"]}}}
DEBUG:urllib3.connectionpool:Starting new HTTP connection (1): 127.0.0.1:41481
DEBUG:urllib3.connectionpool:http://127.0.0.1:41481 "POST /session HTTP/1.1" 200 701
DEBUG:selenium.webdriver.remote.remote_connection:Finished Request
DEBUG:selenium.webdriver.remote.remote_connection:POST http://127.0.0.1:41481/session/1c26f429-dfe3-4f10-aef9-d1d76638b22b/url {"url": "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php"}
DEBUG:urllib3.connectionpool:http://127.0.0.1:41481 "POST /session/1c26f429-dfe3-4f10-aef9-d1d76638b22b/url HTTP/1.1" 200 14
DEBUG:selenium.webdriver.remote.remote_connection:Finished Request
DEBUG:selenium.webdriver.remote.remote_connection:POST http://127.0.0.1:41481/session/1c26f429-dfe3-4f10-aef9-d1d76638b22b/execute/sync {"args": [], "script": "return jQuery.active"}
DEBUG:urllib3.connectionpool:http://127.0.0.1:41481 "POST /session/1c26f429-dfe3-4f10-aef9-d1d76638b22b/execute/sync HTTP/1.1" 500 228
DEBUG:selenium.webdriver.remote.remote_connection:Finished Request
DEBUG:selenium.webdriver.remote.remote_connection:GET http://127.0.0.1:41481/session/1c26f429-dfe3-4f10-aef9-d1d76638b22b/source {}
DEBUG:urllib3.connectionpool:http://127.0.0.1:41481 "GET /session/1c26f429-dfe3-4f10-aef9-d1d76638b22b/source HTTP/1.1" 200 13964
DEBUG:selenium.webdriver.remote.remote_connection:Finished Request
DEBUG:Parser:PARSER END DATA : 7.8°C
8 degrees Celsius
```

### Run Container without DEBUGGING ###
Run Command: `docker run --name poc-parsing poc-parsing:dev`
Example Output:
```
8 degrees Celsius
```

### If container exists from previous run, you need to remove it before running ###
``` 
docker rm --force poc-parsing
```

### Known Issues ###
* There was an issue related to parser retrieving default value before Ajax populated. I made attempt to detect Ajax/jQuery completion. However, if at any time you temperature comes back as weird 2.4C (default value in component) then you know this is still not fully solved and has to be tweaked further.
* Issue ticket and notes: https://gitlab.com/neux/poc-parsing/-/issues/1
