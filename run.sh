
# WITH DEBUG
echo && echo
docker rm --force poc-parsing || true
docker run --name poc-parsing --env DEBUG=TRUE poc-parsing:dev


# WITHOUT DEBUG
echo && echo
docker rm --force poc-parsing || true
docker run --name poc-parsing poc-parsing:dev
