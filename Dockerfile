FROM elgalu/selenium:latest
COPY requirements.txt requirements.txt
COPY app app
RUN pip install --user -r requirements.txt
CMD ["python","app/HoeWarmIsHetInDelft.py"]
